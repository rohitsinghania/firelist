import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

import firebase from "firebase/app";
import "firebase/firestore";

const firebaseConfig = {
  apiKey: "AIzaSyAI4gAWPYHR6XNgBjXvGZVUnZQKCzo5Bc0",
  authDomain: "firelist-c5835.firebaseapp.com",
  projectId: "firelist-c5835",
  storageBucket: "firelist-c5835.appspot.com",
  messagingSenderId: "837293667247",
  appId: "1:837293667247:web:7b88d8c5e61af68977b55a",
  measurementId: "G-F5J5HX9KKX"
};
firebase.initializeApp(firebaseConfig);

const firestore = firebase.firestore();
const settings = { /* your settings... */ timestampsInSnapshots: true };
firestore.settings(settings);


ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
